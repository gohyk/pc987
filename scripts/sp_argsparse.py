# FILE: sp_argsparse.py
import argparse
import math

# Construct an argument parser
all_args = argparse.ArgumentParser()

# Add arguments to the parser
all_args.add_argument("-x", "--X1", required=True, help="x-coord")
all_args.add_argument("-y", "--Y1", required=True, help="y-coord")
args = vars(all_args.parse_args())

distance = math.sqrt(int(args['X1'])**2 + int(args['Y1'])**2)
print(f"Distance from origin = {distance}")
