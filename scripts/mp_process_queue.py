# FILE: mp_process_queue.py
from multiprocessing import Process, Queue
import time

def f(q):
    q.put([42, None, 'hello'])
    q.put(100)
    q.put('last item')

if __name__ == '__main__':
    q = Queue(2)     # the max length of the queue is 2
    p = Process(target=f, args=(q,))
    p.start()
    time.sleep(0.5)
    while not q.empty():
        time.sleep(0.3)
        print(f'Is q full: {q.full():1},  getting a value: {q.get()}')
    p.join()
