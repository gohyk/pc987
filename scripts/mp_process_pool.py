# FILE: mp_process_pool.py
import multiprocessing
from random import random

def inside_circle( x ):
    return 1 if x[0]*x[0] +x[1]*x[1] <= 1 else 0

def setup_task():
    print('Starting ', multiprocessing.current_process().name)

pool_size = multiprocessing.cpu_count() -1
pool      = multiprocessing.Pool(processes=pool_size, initializer=setup_task)

inputs    = [(random(), random()) for _ in range(100000)]  
result    = pool.map(inside_circle, inputs)
pool.close()
pool.join()

ans = 4*sum(result)/len(result)
print( f'pi = {ans:6.4f}')
