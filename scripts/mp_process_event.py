# FILE: mp_process_event.py
import multiprocessing, time, random

whistle = multiprocessing.Event()

def racer(num):
    time.sleep( random.randint(1,8)/3 ) 
    print(f'Racer {num} is ready to race.')
    whistle.wait()
    print('Racer {num} reach the Finish Line!!')

def blow_whistle():
    print("\n*** LET'S START THE RACE ***\n")
    time.sleep(2)
    whistle.set()

for i in range(5):
    multiprocessing.Process(target=racer, args=(i,)).start()

time.sleep(5)
blow_whistle()
