# FILE: mp_process_Rlock.py
import multiprocessing, sys

def task(lock, msg, stream):
    with lock:
        for i in range(100000):
            stream.write(msg)

def writeAstar(lock):
    with lock:
        task(lock, 'A', sys.stdout)
        task(lock, '*', sys.stdout)

lock = multiprocessing.RLock()
p1 = multiprocessing.Process( target=writeAstar, args=(lock,) )
p1.start()
p1.join()

