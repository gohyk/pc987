# FILE: mt_threading_1.py
import threading
import time, random

def worker(num):
    print(f'Worker no. {num} is doing something')
    time.sleep(random.choice([1,2,3]))
    print(f'Worker no. {num} finised')

threads = []
for i in range(3):
    t = threading.Thread(target=worker, args=(i,))
    threads.append(t)
    t.start()
