import subprocess, os
from loguru import logger
from datetime import datetime as dt
from shlex import split

logger.add("error.log",  rotation="500 MB")
git_dir = '/home/kygoh/w/webpages'
cwd = os.getcwd()

@logger.catch
def git_push(git_dir):
    os.chdir(git_dir)
    subprocess.run(split('git add -A'))
    subprocess.run(split('git commit -m "commit on {dt.now()}"'))
    subprocess.run(split('git push origin master'))
    os.chdir(cwd)

git_push(git_dir)
