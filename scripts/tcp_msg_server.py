# FILE: tcp_msg_server.py
import socket

def server_program():
    host = socket.gethostname()
    port = 5000  # initiate port no above 1024

    server_socket = socket.socket()
    server_socket.bind((host, port))

    server_socket.listen(2)
    conn, address = server_socket.accept()
    print("Connection from: " + str(address))
    while True:
        data = conn.recv(1024).decode()
        if not data:
            break

        print("from connected user: " + str(data))
        data = input(' -> ')
        conn.send(data.encode())
    conn.close()

server_program()                
