# FILE: mt_threading_subclass.py
import threading, time, random

def task():
    print('Pretend to do some work')
    time.sleep(random.randint(1,3))

class KissThread(threading.Thread):
    def __init__(self): 
        super().__init__() 
    def run(self):
        task()

for i in range(3):
    KissThread().start() 
