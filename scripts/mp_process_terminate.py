# FILE: mp_process_terminate.py
import multiprocessing, time

def slow_worker():
    print('Starting worker')
    time.sleep(2)
    print('Finished worker')

p = multiprocessing.Process(target=slow_worker)


print(f'BEFORE:    {p.is_alive():2}, {p}')
p.start()
print(f'DURING:    {p.is_alive():2}, {p}')
p.terminate()
print(f'TERMINATED:{p.is_alive():2}, {p}')
p.join()
print(f'JOINED:    {p.is_alive():2}, {p}')
