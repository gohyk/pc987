# FILE: mp_process_condition.py
import multiprocessing, time

def producer(cond):
    print('Kitchen is preparing the food')
    time.sleep(3)
    with cond:
        print('Everyone, dinner is ready!')
        cond.notify_all()

def consumer(cond, num):
    print(f'Consumer {num} is hungry')
    print(f'Consumer {num} ordered food')
    with cond:
        cond.wait()
        print(f'Consumer {num} eating dinner')

cond = multiprocessing.Condition()
p1=multiprocessing.Process(target=consumer,args=(cond,1,))
p2=multiprocessing.Process(target=consumer,args=(cond,2))
p3=multiprocessing.Process(target=producer,args=(cond,))
p1.start();time.sleep(1);  p2.start();time.sleep(1);  p3.start()
