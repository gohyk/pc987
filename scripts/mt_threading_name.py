# FILE: mt_threading_name.py
import threading, time, functools

def task(num, msg):
    name = threading.current_thread().getName()
    print(msg.format(num=num,name=name))
    time.sleep(3)

task1 = functools.partial(task, msg='Task {num}: {name:10s} is farming.')
task2 = functools.partial(task, msg='Task {num}: {name:10s} is mining.')

t1 = threading.Thread(target=task1, args=(1,))  # use default name
t2 = threading.Thread(target=task1, args=(2,), name='Farmer')
t3 = threading.Thread(target=task2, args=(3,), name='Miner')

t1.start()
t2.start()
t3.start()
