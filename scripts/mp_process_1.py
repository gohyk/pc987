# FILE: mp_process_1.py
import multiprocessing, time, random

def worker(num):
    print(f'Worker no. {num+1} is doing something')
    time.sleep(random.randint(1,3))
    print(f'Worker no. {num+1} finished')

processes = []
for i in range(3):
    p = multiprocessing.Process(target=worker, args=(i,))
    processes.append(p)
    p.start()
