# FILE: rmt_copy_files.py
import subprocess
import pathlib

def run_cmd(cmd):
    rtn = subprocess.run(cmd, shell=True)
    print(rtn)

pathlib.Path('./proj').mkdir(exist_ok=True)
for i in range(3):
    with open(f'proj/file_{i}.txt', 'w') as f:
        f.write(f"Add some text here: {chr(100+i)}")
    with open(f'proj/pic_{i}.png', 'w') as f:
        f.write("Dummy pictures")

cmd1 = 'ssh dummy@172.16.195.133 "mkdir -p /home/dummy/proj/"'
cmd2 = 'scp proj/file*.txt dummy@172.16.195.133:/home/dummy/proj/'
cmd3 = 'rsync -r proj dummy@172.16.195.133:/home/dummy/'

for cmd in [cmd1, cmd2, cmd3]:
    run_cmd(cmd)
