# FILE: ap_pathlib_modify.py
import pathlib

myfile = pathlib.Path('level1/level2a/file_20.txt')
print(f"Is myfile exists? {myfile.exists()}")
with open(myfile, 'w') as f:
    msg = "journey of a thousand miles begins with one step"
    f.write(msg)
    print(f"The length of msg: {len(msg)}")
print(f"Is myfile exists? {myfile.exists()}")
print(f"What is the size of myfile? {myfile.stat().st_size} bytes")
print(f"What is myfile extension? {myfile.suffix}")
print(f"What is myfile full path? {myfile.absolute()}")

myfile.unlink()
print(f"Is myfile exists? {myfile.exists()} (is deleted)")

