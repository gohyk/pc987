# FILE: sp_interact_2.py
import subprocess, io

print('All in one go:')
proc = subprocess.Popen( ['python', 'sp_repeater.py'],
    stdin=subprocess.PIPE, stdout=subprocess.PIPE)
stdin  = io.TextIOWrapper(proc.stdin,  encoding='utf8')

for i in range(1,6):
    line = '{} {}\n'.format(i, '*'*i)
    stdin.write(line)
stdin.flush()

output = proc.communicate()[0].decode('utf-8')
print(output)

