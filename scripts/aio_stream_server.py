# FILE: aio_stream_server.py
import asyncio

with open('sample_data.csv','r') as f:
    data_stream = iter(f.read())

async def broadcast(reader, writer):
    message = next(data_stream)
    print(f'Send: {message!r}')
    writer.write(message.encode())
    print('Close the connection')
    writer.close()


async def tcp_echo_server():
    server = await asyncio.start_server(broadcast, '127.0.0.1', 8888)
    async with server:
        await server.serve_forever()


asyncio.run(tcp_echo_server())

