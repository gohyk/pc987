# FILE: sp_run_stdout.py
import subprocess, shlex
cmd  = 'ls -l /etc/hosts'
args = shlex.split(cmd)
task = subprocess.run(args, capture_output=True)
print('Command line output captured from stdout:')
print(task.stdout.decode('utf8'))
