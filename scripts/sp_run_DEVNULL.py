# FILE: sp_run_DEVNULL.py
import subprocess
import shlex

cmd  = 'echo msg to stdout; invalid command'
args = shlex.split(cmd)

try:
    task = subprocess.run( cmd, shell=True, #check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL)
except subprocess.CalledProcessError as err:
    print(f'ERROR: {err}')
else:
    print(f"returncode: {task.returncode}")
    print(f"stdout msg: {task.stdout}")
    print(f"stderr msg: {task.stderr}")
