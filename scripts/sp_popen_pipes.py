# FILE: sp_popen_pipes.py
import subprocess

tar  = subprocess.Popen(['tar', 'ztf', 'Downloads.tar.gz'],
        stdout = subprocess.PIPE)

grep = subprocess.Popen(['grep', 'py$'],
        stdin=tar.stdout,stdout=subprocess.PIPE)

wc   = subprocess.Popen(['wc', '-l'],
        stdin=grep.stdout,stdout=subprocess.PIPE)

end_of_pipe = wc.stdout.read()
print('Number of *.py files is %s'%end_of_pipe.decode('utf8'))
