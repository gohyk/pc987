# FILE: sp_interact_1.py
import subprocess, io

print('One line at a time:')
proc = subprocess.Popen( ['python', 'sp_repeater.py'],
    stdin=subprocess.PIPE, stdout=subprocess.PIPE)
stdout = io.TextIOWrapper(proc.stdout, encoding='utf8')
stdin  = io.TextIOWrapper(proc.stdin,  encoding='utf8',
    line_buffering=True,)  # send data on newline
for i in range(1,6):
    line = '{} {}\n'.format(i, '*'*i)
    stdin.write(line)
    output = stdout.readline()
    print(output.rstrip())

remainder = proc.communicate()[0].decode('utf8')
print('Nothing left in remainder: ',remainder, '[DONE]')
