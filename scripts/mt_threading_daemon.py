# FILE: mt_threading_daemon.py
import threading, time, functools, logging

def task(delay):
    logging.debug( f'Pretending to work for {delay} seconds.' )
    time.sleep(delay)

logging.basicConfig( level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-6s) %(message)s',)

d = threading.Thread(target=task, args=(5,), name='Daemon', daemon=True)
m = threading.Thread(target=task, args=(1,), name='Mortal')

d.start(); m.start()
msg ='Daemon is alive: %-5s  | Mortal is alive: %-5s'
print( msg%(d.is_alive(), m.is_alive()) )
m.join()    # join( n ) will timeout even if m is not complete
print( msg%(d.is_alive(), m.is_alive()) )
d.join()
print( msg%(d.is_alive(), m.is_alive()) )
