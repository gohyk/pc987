# FILE: mt_threading_logging.py
import threading, time, functools, logging

def task(num, msg):
    name = threading.current_thread().getName()
    logging.debug(msg%(num,name))
    time.sleep(1)

logging.basicConfig( level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',)

task1 = functools.partial(task, msg='Task %d: %10s is farming.')
task2 = functools.partial(task, msg='Task %d: %10s is mining.') 

t1 = threading.Thread(target=task1, args=(1,))  # use default name
t2 = threading.Thread(target=task1, args=(2,), name='Farmer')
t3 = threading.Thread(target=task2, args=(3,), name='Miner')

t1.start(); t2.start(); t3.start()
