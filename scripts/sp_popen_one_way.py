# FILE: sp_popen_one_way.py
import subprocess

cmd  = 'echo msg1; echo msg2; echo msg3'
proc = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE)
proc_output = proc.communicate()
print('Output is a tuple of (stdout, stderr).')
print(proc_output)

cmd  = 'cat -'  # concatenate to screen
proc = subprocess.Popen( cmd, shell=True, stdin=subprocess.PIPE)
input_stream = 'msg send to subprocess.PIPE\n'
print('\nSending msg to stdin ...')
proc.communicate(input_stream.encode('utf8'))

