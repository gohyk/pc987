# FILE: mt_threading_lock2.py
import threading

x=0
lock = threading.Lock()
def step(num):
    global x
    with lock:
        for i in range(1000000): 
            x= x + num

t1 = threading.Thread(target=step, args=(1,))
t2 = threading.Thread(target=step, args=(-1,))
t1.start(); t2.start()
t1.join();  t2.join()
print(x)
