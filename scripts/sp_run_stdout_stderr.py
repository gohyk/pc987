# FILE: sp_run_stdout_stderr.py
import subprocess
import shlex

cmd  = 'echo msg to stdout; invalid command'
args = shlex.split(cmd)

try:
    task = subprocess.run( cmd, shell=True, #check=True,
        capture_output=True)
except subprocess.CalledProcessError as err:
    print(f'ERROR: {err}')
else:
    print(f"returncode: {task.returncode}")
    print(f"stdout msg: {task.stdout.decode('utf8')}")
    print(f"stderr msg: {task.stderr.decode('utf8')}")

