# FILE: mt_thread.py
import _thread, time, os, signal, sys

def task( threadName, delay):
    for i in range(3):
        time.sleep(delay)
        print(f'{threadName}, {os.getpid()}' ))

def signal_handler(signum, stack): 
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
_thread.start_new_thread( task, ("Thread #1", 1, ) )
_thread.start_new_thread( task, ("Thread #2", 2, ) )

print('Press Ctrl-C to exit')
while True:
    pass

