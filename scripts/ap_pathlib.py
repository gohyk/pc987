# FILE: ap_zip_archive.py
import pathlib

pick = ['.txt', '.md', '.pdf', '.py']

def make_archive(dir_name, pick):
    import zipfile
    p = pathlib.Path(dir_name)
    with zipfile.ZipFile(f'{p.name}.zip', 'w') as zf:
        for fname in p.rglob('*'):
            if fname.suffix in pick:
                print(f'Adding {fname} ...')
                zf.write(fname)
    print("Archiving ... ... ... [Done]")

make_archive('scripts', pick)
