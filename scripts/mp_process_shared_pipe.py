# FILE: mp_process_shared_pipe.py
import multiprocessing, sys

def task(msg, stream):
    for i in range(100000):
        stream.write(msg)

p1 = multiprocessing.Process(target=task, args=('A', sys.stdout))
p2 = multiprocessing.Process(target=task, args=('*', sys.stdout))

p1.start()
p2.start()
p1.join()
p2.join()

