# FILE: mp_process_shared_list.py
import multiprocessing, time, os, random

def task(l):
    l.append(os.getpid())
    time.sleep(random.randint(1,2))

mgr  = multiprocessing.Manager()
pids = mgr.list()   # create a shared list

jobs = [ multiprocessing.Process(target=task, args=(pids, ))
         for i in range(10)]

[j.start() for j in jobs]
[j.join()  for j in jobs]
print(pids)
